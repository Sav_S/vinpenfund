# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_blogindexrelatedlink_intro'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogindexrelatedlink',
            name='date',
            field=models.DateField(verbose_name='Post date', default="2015-12-12"),
            preserve_default=False,
        ),
    ]
