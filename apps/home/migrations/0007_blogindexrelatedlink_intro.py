# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0006_blogindexrelatedlink'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogindexrelatedlink',
            name='intro',
            field=models.CharField(default=1991, help_text='Link title', max_length=250),
            preserve_default=False,
        ),
    ]
