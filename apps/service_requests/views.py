from django.shortcuts import render
from django.http import HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.utils.encoding import force_text
from .forms.forms import DeadlineDetailForm, RequestDetailForm, CompletionDetailForm
import json


def index_test(request):
    return HttpResponse("Test views")


# Base Page
def get_managment(request):
    return render(request, 'service_requests/management.html')


# list all requests
@login_required(login_url='login')
def get_requests_list(request):
    return render(request, 'service_requests/requestlist.html')


# get request details
@login_required(login_url='login')
def get_request_details(request):
    return render(request, 'service_requests/requestdetails.html')


# get request form for edit
@login_required(login_url='login')
def get_edit_request(request):
    return render(request, 'service_requests/editrequest.html')


# get deadline details (for setting deadline & responsible employees)
@login_required(login_url='login')
def get_deadline_details(request):
    return render(request, 'service_requests/deadline.html')


# get completion details(for employees)
@login_required(login_url='login')
def get_completion_details(request):
    return render(request,'service_requests/completion.html')



# Page after save valid form
class NgFormDataValidView(generic.TemplateView):
    template_name = 'service_requests/requestvalid.html'


# Form Views
class TemplateFormView():
    def ajax(self, request):
        form = self.form_class(data=json.loads(request.body))
        response_data = {'errors': form.errors, 'success_url': force_text(self.success_url)}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class CreateRequestFormView(generic.FormView, TemplateFormView):
    template_name = "service_requests/createrequest.html"
    form_class = RequestDetailForm
    success_url = reverse_lazy('form_data_valid')


class DeadlineDetailFormView(generic.FormView, TemplateFormView):
    template_name = "service_requests/abstractform.html"
    form_class = DeadlineDetailForm


class RequestDetailFormView(generic.FormView, TemplateFormView):
    template_name = "service_requests/abstractform.html"
    form_class = RequestDetailForm


class CompletionDetailFormView(generic.FormView, TemplateFormView):
    template_name = "service_requests/abstractform.html"
    form_class = CompletionDetailForm
