# -*- coding: utf-8 -*-
from django.db import models


class ServiceRequests(models.Model):
    username = models.CharField(max_length=255)
    cabinet = models.CharField(max_length=5)
    problem = models.CharField(max_length=255)
    phone = models.CharField(max_length=11, null=True)
    location = models.CharField(max_length=1)
    date_request = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        ordering = ('-date_request',)


class Deadlines (models.Model):
    request = models.OneToOneField('ServiceRequests', on_delete=models.CASCADE, null=True)
    responsible_employee = models.CharField(max_length=255)
    deadline = models.DateField()

    class Meta:
        managed = True
        ordering = ('-deadline',)


class InfoCompletion(models.Model):
    request = models.OneToOneField('ServiceRequests', on_delete=models.CASCADE, null=True)
    date_completion = models.DateField()
    worklist = models.CharField(max_length=255, null=True)

    class Meta:
        managed = True
        ordering = ('-date_completion',)

