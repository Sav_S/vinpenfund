# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_requests', '0003_auto_20151221_0821'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deadlines',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests', null=True),
        ),
        migrations.AlterField(
            model_name='infocompletion',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests', null=True),
        ),
    ]
