# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_requests', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deadlines',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests'),
        ),
        migrations.AlterField(
            model_name='infocompletion',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests'),
        ),
    ]
