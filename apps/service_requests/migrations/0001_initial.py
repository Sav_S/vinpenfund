# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Deadlines',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('responsible_employee', models.CharField(max_length=255)),
                ('deadline', models.DateField()),
            ],
            options={
                'managed': True,
                'ordering': ('-deadline',),
            },
        ),
        migrations.CreateModel(
            name='InfoCompletion',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('date_completion', models.DateField()),
                ('worklist', models.CharField(max_length=255, null=True)),
            ],
            options={
                'managed': True,
                'ordering': ('-date_completion',),
            },
        ),
        migrations.CreateModel(
            name='ServiceRequests',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('username', models.CharField(max_length=255)),
                ('cabinet', models.CharField(max_length=5)),
                ('problem', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=11, null=True)),
                ('location', models.CharField(max_length=1)),
                ('date_request', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'managed': True,
                'ordering': ('-date_request',),
            },
        ),
        migrations.AddField(
            model_name='infocompletion',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests', null=True),
        ),
        migrations.AddField(
            model_name='deadlines',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests', null=True),
        ),
    ]
