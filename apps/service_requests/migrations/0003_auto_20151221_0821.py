# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_requests', '0002_auto_20151208_1039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deadlines',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests', null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='infocompletion',
            name='request',
            field=models.OneToOneField(to='service_requests.ServiceRequests', null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
    ]
