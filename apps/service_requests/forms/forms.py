from djangular.forms import NgFormValidationMixin, NgModelFormMixin
from . import bootstrap_forms


class DeadlineDetailForm(NgModelFormMixin, NgFormValidationMixin, bootstrap_forms.DeadlineDetailForm):
    scope_prefix = 'request_data'
    form_name = 'deadline_form'


class RequestDetailForm(NgModelFormMixin, NgFormValidationMixin, bootstrap_forms.RequestDetailForm):
    scope_prefix = 'request_data'
    form_name = 'request_form'


class CompletionDetailForm(NgModelFormMixin, NgFormValidationMixin, bootstrap_forms.CompletionDetailForm):
    scope_prefix = 'request_data'
    form_name = 'completion_form'