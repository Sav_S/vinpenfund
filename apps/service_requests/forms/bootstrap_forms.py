from django import forms
from djangular.styling.bootstrap3.forms import Bootstrap3Form
from django.contrib.auth.models import User


class UserLocationForm(Bootstrap3Form):
    locations = [("v", "Вишенька"), ("z", "Замостя"), ("k", "Книжка")]
    problem = forms.CharField(label='Проблема *', max_length=255, widget=forms.Textarea(
        attrs={'cols': '80', 'rows': '4'}), required=True, help_text='Короткий опис проблеми, яку потрібно вирішити')
    location = forms.ChoiceField(choices=locations, label='Місцезнаходження *', required=True)


class UserContactForm(Bootstrap3Form):
    username = forms.CharField(label='Користувач *', max_length=255, required=True,
                               help_text='П.І.Б користувача наприклад - Іванов І.І')
    phone = forms.CharField(label='Контактний телефон', max_length=11, required=False)
    cabinet = forms.CharField(label='Кабінет *', max_length=5, required=True)


class CompletionDetailForm(Bootstrap3Form):
    date_completion = forms.DateField(label='Дата виконання', help_text='Дата в форматі: рррр-мм-дд', required=False,
                               widget=forms.DateInput(attrs={'is-open': 'status.opencomp',
                                                             'ui-bootstrap-datepicker': '',
                                                             'uib-datepicker-popup': 'yyyy-MM-dd',
                                                             'current-text': 'Сьогодні',
                                                             'clear-text': 'Очистити',
                                                             'close-text': 'Закрити'
                                                             }))
    worklist = forms.CharField(label='Проведені роботи', max_length=255, widget=forms.Textarea(
                               attrs={'cols': '80', 'rows': '3'}),
                               required=False,)


class RequestDetailForm(UserContactForm, UserLocationForm):
    pass


class DeadlineDetailForm(Bootstrap3Form):
    employees = (("".join([c.last_name, " ", c.first_name[0], "."]), " ".join([c.last_name, c.first_name]))for c in
                 User.objects.all())
    deadline = forms.DateField(label='Контрольна дата', help_text='Дата в форматі: рррр-мм-дд',
                               required=False,
                               widget=forms.DateInput(attrs={'is-open': 'status.opened',
                                                             'ui-bootstrap-datepicker': '',
                                                             'type-datepicker': 'deadline',
                                                             'uib-datepicker-popup': 'yyyy-MM-dd',
                                                             'current-text': 'Сьогодні',
                                                             'clear-text': 'Очистити',
                                                             'close-text': 'Закрити'
                                                             })
                               )
    employee = forms.ChoiceField(choices=employees, label='Закріплений працівник', required=False,
                                             widget=forms.Select(attrs={'ng-change': 'update()'}))



