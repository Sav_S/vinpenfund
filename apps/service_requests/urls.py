from django.conf.urls import url
from django.views.generic import TemplateView
from .views import *


urlpatterns = [
    url(r'^test/$', index_test, name='test'),
    url(r'^$', get_managment, name='request_managment'),
    url(r'^create/$', CreateRequestFormView.as_view(), name='create_request_form'),
    url(r'^valid/$', NgFormDataValidView.as_view(), name='form_data_valid'),
    url(r'^list/$', get_requests_list, name='request_managment'),
    url(r'^requestdetails/$', get_request_details, name='request_details'),
    url(r'^editrequest/$', get_edit_request, name='edit_request'),
    url(r'^deadlinedetails/$', get_deadline_details, name='deadline_details'),
    url(r'^completiondetails/$', get_completion_details, name='completion_details'),

    # Modal forms
    url(r'^deadlineform/$', DeadlineDetailFormView.as_view(), name='deadline_form'),
    url(r'^requestform/$', RequestDetailFormView.as_view(), name='user_request_form'),
    url(r'^completionform/$', CompletionDetailFormView.as_view(), name='completion_form'),

    # Date picker http://angular-ui.github.io/bootstrap/#/top
    url(r'^datepicker/$', TemplateView.as_view(template_name='service_requests/datepicker.html'), name='datepicker'),
    url(r'^stdate/$', TemplateView.as_view(template_name='service_requests/stDateRange.html'), name='daterange'),

]