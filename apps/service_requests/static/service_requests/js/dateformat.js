/**
 * Created by SergSav on 14.10.2015.
 * return date in format yyyy-mm-dd or null
 */
var dateparse = function(date){
    // date in Date() format
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var convertedDate = year + "-" + month + "-" + day;
    return convertedDate;
}
var isodateformat = function (date) {
    if(typeof date == 'string') {
        //var dateParts = date.split(".");
        //var parseDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
        //var isoDate = dateparse(parseDate);
        var isoDate = date;
    }
    else if (date== null) {
        var isoDate = null;
    }
    else if (typeof date.getDate() == 'number' ) {
        var isoDate =  dateparse(date);

    }

    return isoDate;
};
var gostdateformat = function(date){
    if (date){
       var parseDate = new Date(Date.parse(date));
       var day = parseDate.getDate();
       var month = parseDate.getMonth() + 1;
       var year = parseDate.getFullYear();
       var gostDate = day + "." + month + "." + year;
    }
    else{
      var gostDate = null
    }
    return gostDate;
}