/*jslint devel: true    */
(function (ng) {
    'use strict';
    var mngApp = angular.module('RequestManagement', ['ui.router', 'restangular', 'ng.django.forms', 'smart-table',
            'ui.bootstrap', 'ngAnimate', 'ngAside']);
    /* Config */
    mngApp.config(
        function ($httpProvider, $stateProvider, $urlRouterProvider, RestangularProvider) {
            $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
            RestangularProvider.setBaseUrl('/api/v1/service_requests/');
            $urlRouterProvider.otherwise('/create');
            $stateProvider
                .state('parent', {
                    url: 'servicerequests/',
                    abstract: true,
                    template: '<ui-view/>'
                })
                .state('create', {
                    url: "/create",
                    templateUrl: "/servicerequests/create/",
                    controller: "CreateCtrl"
                })
                .state('valid', {
                    url: "/valid",
                    templateUrl: "/servicerequests/valid/"
                })
                .state('list', {
                    url: "/list",
                    templateUrl: "/servicerequests/list/",
                    controller: "RequestListCtrl"
                });

        }
    );

    /* Controllers */

    // New request
    mngApp.controller('CreateCtrl', function ($scope, $http, $state, djangoForm, Restangular) {
        $scope.submit = function () {
            if ($scope.request_data) {
                $http.post(".", $scope.request_data).success(function (out_data) {
                    if (!djangoForm.setErrors($scope.request_form, out_data.errors)) {
                        //on successful post, save data and redirect
                        Restangular.all('requests/').post($scope.request_data).then(function (addedRequest) {
                            $state.go('valid');
                        }, function () {
                            $scope.result = {
                                message: "create failed"
                            };
                        });
                    }
                }).error(function () {
                    console.error('An error occured during submission test');
                });
            }
            return false;
        };
    });
    // List all requests and modal dialogs
    mngApp.controller('RequestListCtrl', function ($scope, $uibModal, $http, $aside, djangoForm, Restangular) {
        $scope.rowCollection = Restangular.all('expanded/').getList().$object;
        $scope.itemsByPage = 8;
        $scope.displayedCollection = [].concat($scope.rowCollection);
        // Delete row
        $scope.removeItem = function removeItem(row) {
            var index, data_remove;
            index = $scope.rowCollection.indexOf(row);
            if (index !== -1) {
                // Delete from database
                Restangular.one('requests', row.id).get().then(function (c) {
                    data_remove = c;
                    data_remove.remove().then(function () {
                        // Delete from rowCollections
                        $scope.rowCollection.splice(index, 1);
                        $scope.addAlert('Запит успішно видалено з БД', 'success');
                    }, function () {
                        $scope.addAlert('Помилка при видаленні запису', 'danger');
                    });
                });
                //data_remove = Restangular.copy(row);
                //data_remove.remove().then(function () {

            }

        };
        // Alerts
        $scope.alerts = [];
        $scope.addAlert = function (msg_value, type_value) {
            $scope.alerts.push({
                msg: msg_value,
                type: type_value
            });
        };
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.isCollapsed = true;
        // Modal window for employees
        $scope.completionState = {
            open: false
        };
        $scope.openCompletion = function (position, backdrop, details) {
            $scope.completionState = {
                open: true,
                position: position
            };

            function postDismiss() {
                $scope.completionState.open = false;
                console.log('Modal dismissed at: ' + new Date());
            }
            $aside.open({
                templateUrl: '/servicerequests/completiondetails/',
                placement: position,
                // size: 'sm',
                backdrop: backdrop,
                controller: 'CompletionDetailCtrl',
                resolve: {
                    request_details: function () {
                        return details;
                    }
                }
            }).result.then(
                function (new_values) {
                    $scope.completionState.open = false;
                    var index, new_completion_info, edit_completion_info;
                    index = $scope.rowCollection.indexOf(details);
                    if (index !== -1) {
                        // try saving data
                        if (new_values.completion_id === null) {
                            // new information of completion
                            new_completion_info = {
                                request: new_values.id,
                                date_completion: isodateformat(new_values.date_completion),
                                worklist: new_values.worklist
                            };
                            Restangular.all('completion/').post(new_completion_info).then(function (c) {
                                $scope.addAlert('Інформація внесена до БД', 'success');
                                new_values.completion_id = c.id;
                                new_values.request_status = request_status(new_values.deadline,
                                    new_values.date_completion,
                                    new_values.employee);
                                ng.extend(details, new_values);

                            }, function () {
                                $scope.addAlert('Помилка при внесенні інформації', 'danger');
                            });
                        } else {
                            Restangular.one('completion', new_values.completion_id).get().then(function (c) {
                                edit_completion_info = c;
                                edit_completion_info.date_completion = isodateformat(new_values.date_completion);
                                edit_completion_info.worklist = new_values.worklist;
                                edit_completion_info.put().then(function () {
                                    $scope.addAlert('Зміни успішно записані', 'success');
                                    new_values.request_status = request_status(new_values.deadline,
                                        new_values.date_completion,
                                        new_values.employee);
                                    ng.extend(details, new_values);
                                }, function () {
                                    $scope.addAlert('Помилка при збереженні інформації', 'danger');
                                });
                            });
                        }
                    }
                }, postDismiss);
        };
        // Modal window for admin (set deadline and employee)
        $scope.settingDeadlineState = {
            open: false
        };
        $scope.openSettingDeadline = function (position, backdrop, details) {
            $scope.settingDeadlineState = {
                open: true,
                position: position,
            };

            function postDismiss() {
                $scope.settingDeadlineState.open = false;
                console.log('settingDeadline dismissed at: ' + new Date());
            }
            $aside.open({
                templateUrl: '/servicerequests/deadlinedetails/',
                placement: position,
                // size: 'sm',
                backdrop: backdrop,
                controller: 'SettingDeadlineCtrl',
                resolve: {
                    request_details: function () {
                        return details;
                    }
                }
            }).result.then(
                function (new_values) {
                    $scope.settingDeadlineState.open = false;
                    var index, new_deadline, edit_deadline;
                    index = $scope.rowCollection.indexOf(details);
                    if (index !== -1) {
                        // try saving data
                        if (new_values.deadline_id === null) {
                            new_deadline = {
                                request: new_values.id,
                                deadline: isodateformat(new_values.deadline),
                                responsible_employee: new_values.employee
                            };
                            Restangular.all('deadlines/').post(new_deadline).then(function (c) {
                                $scope.addAlert('Інформація внесена до БД', 'success');
                                new_values.deadline_id = c.id;
                                new_values.request_status = request_status(new_values.deadline,
                                    new_values.date_completion,
                                    new_values.employee);
                                ng.extend(details, new_values);
                                
                            }, function () {
                                $scope.addAlert('Помилка при внесенні інформації', 'danger');
                            });
                        } else {
                            Restangular.one('deadlines', new_values.deadline_id).get().then(function (c) {
                                edit_deadline = c;
                                edit_deadline.deadline = isodateformat(new_values.deadline);
                                edit_deadline.responsible_employee = new_values.employee;
                                edit_deadline.put().then(function () {
                                    $scope.addAlert('Зміни успішно записані', 'success');
                                    new_values.request_status = request_status(new_values.deadline,
                                        new_values.date_completion,
                                        new_values.employee);
                                    ng.extend(details, new_values);

                                }, function () {
                                    $scope.addAlert('Помилка при збереженні інформації', 'danger');
                                });
                            });
                        }
                    }
                }, postDismiss);
        };
        $scope.editrequestState = {
            open: false
        };
        $scope.openEditRequest = function (position, backdrop, details) {
            $scope.editrequestState = {
                open: true,
                position: position,
            };

            function postDismiss() {
                $scope.editrequestState.open = false;
                console.log('editRequest dismissed at: ' + new Date());
            }
            $aside.open({
                templateUrl: '/servicerequests/editrequest/',
                placement: position,
                // size: 'sm',
                backdrop: backdrop,
                controller: 'EditRequestCtrl',
                resolve: {
                    request_details: function () {
                        return details;
                    }
                }
            }).result.then(
                function (new_values) {
                    $scope.editrequestState.open = false;
                    var index, edit_request;
                    index = $scope.rowCollection.indexOf(details);
                    if (index !== -1) {
                        // try saving data
                            Restangular.one('requests', new_values.id).get().then(function (c) {
                                edit_request = c;
                                edit_request.username = new_values.username;
                                edit_request.cabinet = new_values.cabinet;
                                edit_request.problem = new_values.problem;
                                edit_request.location = new_values.location;
                                edit_request.phone = new_values.phone;
                                edit_request.put().then(function () {
                                    $scope.addAlert('Зміни успішно записані', 'success');
                                    ng.extend(details, new_values);
                                }, function () {
                                    $scope.addAlert('Помилка при збереженні інформації', 'danger');
                                });
                            });
                    }
                }, postDismiss);
        };
        
        $scope.animationsEnabled = true;
    });
    // setingDeadline modal window controller
    mngApp.controller('SettingDeadlineCtrl', function ($scope, $http, $uibModalInstance, Restangular, djangoForm, request_details) {
        var original = request_details;
        $scope.details = original;
        $scope.status = {
            opened: false,
        };
        $scope.request_data = Restangular.copy(original);
        // Check changes
        $scope.isClean = function () {
            return angular.equals(original, $scope.request_data);
        };
        // Ui-bootstrap deadline datapicker  open
        $scope.open_dd = function ($event) {
            $scope.status.opened = true;
        };
        // Validate form method http://djangular.aws.awesto.com/combined_validation/
        // Value my_form need to bypass bug in ui-bootstrap.
        // https://github.com/angular-ui/bootstrap/issues/969
        // fixed in ui-bootstrap 0.12.
        $scope.submit_deadline_form = function (deadline_form) {
            if ($scope.request_data) {
                $http.post(".", $scope.request_data).success(function (out_data) {
                    if (!djangoForm.setErrors(deadline_form, out_data.errors)) {
                        //on successful post return request_data for update
                        $uibModalInstance.close($scope.request_data);
                    }

                }).error(function () {
                    console.error('Error occured during completion form submission test');
                });
            }
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    });
    // Completion info modal window controller
    mngApp.controller('CompletionDetailCtrl', function ($scope, $http, $uibModalInstance, Restangular, djangoForm, request_details) {
        var original = request_details;
        $scope.details = original;
        $scope.status = {
            opencomp: false
        };
        $scope.request_data = Restangular.copy(original);
        // Check changes
        $scope.isClean = function () {
            return angular.equals(original, $scope.request_data);
        };
        // Ui-bootstrap completion datapicker  open
        $scope.open_com = function ($event) {
            $scope.status.opencomp = true;
        };
        // Validate form method http://djangular.aws.awesto.com/combined_validation/
        // Value my_form need to bypass bug in ui-bootstrap.
        // https://github.com/angular-ui/bootstrap/issues/969
        // fixed in ui-bootstrap 0.12.
        $scope.submit_completion_form = function (completion_form) {
            if ($scope.request_data) {
                $http.post(".", $scope.request_data).success(function (out_data) {
                    if (!djangoForm.setErrors(completion_form, out_data.errors)) {
                        //on successful post return request_data for update
                        $uibModalInstance.close($scope.request_data);
                    }
                }).error(function () {
                    console.error('Error occured during completion form submission test');
                });
            }
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    });
  mngApp.controller('EditRequestCtrl', function ($scope, $http, $uibModalInstance, Restangular, djangoForm, request_details) {
        var original = request_details;
        $scope.details = original;
        $scope.request_data = Restangular.copy(original);
        // Check changes
        $scope.isClean = function () {
            return angular.equals(original, $scope.request_data);
        };
        $scope.submit_request_form = function (request_form) {
            if ($scope.request_data) {
                $http.post(".", $scope.request_data).success(function (out_data) {
                    if (!djangoForm.setErrors(request_form, out_data.errors)) {
                        //on successful post return request_data for update
                        $uibModalInstance.close($scope.request_data);
                    }
                }).error(function () {
                    console.error('Error occured during completion form submission test');
                });
            }
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    });
    
    
    
    /* Directives */
    mngApp.directive('stDateRange', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            require: '^stTable',
            scope: {
                before: '=',
                after: '='
            },
            templateUrl: '/servicerequests/stdate/',

            link: function (scope, element, attr, table) {

                var inputs = element.find('input');
                var inputBefore = ng.element(inputs[0]);
                var inputAfter = ng.element(inputs[1]);
                var predicateName = attr.predicate;


                [inputBefore, inputAfter].forEach(function (input) {

                    input.bind('blur', function () {


                        var query = {};

                        if (!scope.isBeforeOpen && !scope.isAfterOpen) {

                            if (scope.before) {
                                query.before = scope.before;
                            }

                            if (scope.after) {
                                query.after = scope.after;
                            }

                            scope.$apply(function () {
                                table.search(query, predicateName);
                            });
                        }
                    });
                });

                function open(before) {
                    return function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();

                        if (before) {
                            scope.isBeforeOpen = true;
                        } else {
                            scope.isAfterOpen = true;
                        }
                    };
                }

                scope.openBefore = open(true);
                scope.openAfter = open();
            }
        };
    }]);

    mngApp.directive('uiBootstrapDatepicker', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ngModelCtrl) {
                element.wrap('<div class="input-group"></div>');

                if (attrs.name == 'deadline') {
                    $compile('<span class="input-group-btn"><button type="button" class="btn btn-default" ng-click="open_dd($event)"> ' +
                        '<i class="glyphicon glyphicon-calendar"></i></button></span>')(scope, function (cloned, scope) {
                        element.after(cloned);
                    });
                } else if (attrs.name == 'date_completion') {
                    $compile('<span class="input-group-btn"><button type="button" class="btn btn-default" ng-click="open_com($event)"> ' +
                        '<i class="glyphicon glyphicon-calendar"></i></button></span>')(scope, function (cloned, scope) {
                        element.after(cloned);
                    });

                }
            }
        };
    });

    mngApp.directive('deadlineForm', function () {
        return {
            restrict: 'E',
            templateUrl: '/servicerequests/deadlineform/'
        };
    });

    mngApp.directive('requestForm', function () {
        return {
            restrict: 'E',
            templateUrl: '/servicerequests/requestform/'
        };
    });

    mngApp.directive('completionForm', function () {
        return {
            restrict: 'E',
            templateUrl: '/servicerequests/completionform/'
        };
    });

    /* Filter */
    mngApp.filter('customFilter', ['$filter', function ($filter) {
        var filterFilter = $filter('filter');
        var standardComparator = function standardComparator(obj, text) {
            text = ('' + text).toLowerCase();
            return ('' + obj).toLowerCase().indexOf(text) > -1;
        };

        return function customFilter(array, expression) {
            function customComparator(actual, expected) {

                var isBeforeActivated = expected.before;
                var isAfterActivated = expected.after;
                var isLower = expected.lower;
                var isHigher = expected.higher;
                var higherLimit;
                var lowerLimit;
                var itemDate;
                var queryDate;


                if (ng.isObject(expected)) {

                    //date range
                    if (expected.before || expected.after) {
                        try {
                            if (isBeforeActivated) {
                                higherLimit = expected.before;

                                itemDate = new Date(actual);
                                queryDate = new Date(higherLimit);

                                if (itemDate > queryDate) {
                                    return false;
                                }
                            }

                            if (isAfterActivated) {
                                lowerLimit = expected.after;


                                itemDate = new Date(actual);
                                queryDate = new Date(lowerLimit);

                                if (itemDate < queryDate) {
                                    return false;
                                }
                            }

                            return true;
                        } catch (e) {
                            return false;
                        }

                    } else if (isLower || isHigher) {
                        //number range
                        if (isLower) {
                            higherLimit = expected.lower;

                            if (actual > higherLimit) {
                                return false;
                            }
                        }

                        if (isHigher) {
                            lowerLimit = expected.higher;
                            if (actual < lowerLimit) {
                                return false;
                            }
                        }

                        return true;
                    }
                    //etc

                    return true;

                }
                return standardComparator(actual, expected);
            }

            var output = filterFilter(array, expression, customComparator);
            return output;
        };
    }]);
})(angular);