/**
 * Created by SergSav on 11.11.2015.
 * return status of user request
 */

var request_status = function(deadline, date_completion, employee){
    status = "active";
    var now = new Date();
    if (deadline != null){
        if (typeof deadline == "string" ){
        deadline = new Date(deadline);
        }
    }
    if (date_completion != null){
        if (typeof date_completion == "string"){
         date_completion = new Date(date_completion);
        }
    }
    if (deadline == null || employee == null){
        status = "info";
    }
    else if (deadline != null && deadline < now && date_completion == null){
        status = "danger";
    }
    else if (deadline != null && date_completion != null && deadline >= date_completion && employee != null){
        status = "success";
    }
    else if (deadline != null && date_completion != null && deadline < date_completion && employee != null){
        status = "warning";
    }
    return status;
}
