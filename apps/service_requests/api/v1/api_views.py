from django.http import HttpResponse
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from django.db.models import CharField, Case, Value, When, Q, F
from datetime import date
from ...models import ServiceRequests, Deadlines, InfoCompletion
from .serializer import ServiceRequestsSerializer, DeadlinesSerializer, InfoCompletionSerializer, ExpandedListSerializer


def index_test(request):
    return HttpResponse("Test views")


class ServiceRequestsList(generics.ListCreateAPIView):
    model = ServiceRequests
    serializer_class = ServiceRequestsSerializer

    def get_queryset(self):
        queryset = ServiceRequests.objects.all()
        return queryset


@permission_classes((IsAdminUser,))
class DeadlinesList(generics.ListCreateAPIView):
    model = Deadlines
    serializer_class = DeadlinesSerializer

    def get_queryset(self):
        queryset = Deadlines.objects.all()
        return queryset


@permission_classes((IsAuthenticated,))
class InfoCompletionList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated)
    model = InfoCompletion
    serializer_class = InfoCompletionSerializer

    def get_queryset(self):
        queryset = InfoCompletion.objects.all()
        return queryset


@permission_classes((IsAuthenticated,))
class ExpandedList(viewsets.ReadOnlyModelViewSet):
    model = ServiceRequests
    serializer_class = ExpandedListSerializer

    def get_queryset(self):
        queryset = ServiceRequests.objects.annotate(
            location_value=Case(
                When(Q(location='k'), then=Value(u'Книжка')),
                When(Q(location='v'), then=Value(u'Вишенька')),
                When(Q(location='z'), then=Value(u'Замостя')),
                output_field=CharField(),
            ),
            request_status=Case(
                # __lte - less or equal
                # danger - red
                # warning - yellow
                # success - green
                # info - blue
                When(Q(deadlines__deadline=None) | Q(deadlines__responsible_employee=None), then=Value(u'info')),
                When(Q(deadlines__deadline__lt=date.today()) & Q(infocompletion__date_completion=None),
                     then=Value(u'danger')),
                When(deadlines__deadline__lt=F('infocompletion__date_completion'), then=Value(u'warning')),
                When(deadlines__deadline__gte=F('infocompletion__date_completion'), then=Value(u'success')),
                output_field=CharField(),
                default=Value(u'active'),
            )
        )
        return queryset


@permission_classes((IsAdminUser,))
class ServiceRequestsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ServiceRequests.objects.all()
    serializer_class = ServiceRequestsSerializer


@permission_classes((IsAdminUser,))
class DeadlineDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Deadlines.objects.all()
    serializer_class = DeadlinesSerializer


@permission_classes((IsAuthenticated,))
class InfoCompletionDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = InfoCompletion.objects.all()
    serializer_class = InfoCompletionSerializer