from django.conf.urls import url
from .api_views import *

urlpatterns = [
    #list
    url(r'^test/$', index_test, name='test'),
    url(r'^requests/$', ServiceRequestsList.as_view(), name='service_requests_list'),
    url(r'^deadlines/$', DeadlinesList.as_view(), name='deadlines_list'),
    url(r'^completion/$', InfoCompletionList.as_view(), name='infocompletions_list'),
    url(r'^expanded/$', ExpandedList.as_view({'get': 'list'}), name='expanded_list'),
    # Detail
    url(r'^requests/(?P<pk>[0-9]+)/$', ServiceRequestsDetail.as_view(), name='service_request_details'),
    url(r'^deadlines/(?P<pk>[0-9]+)/$', DeadlineDetails.as_view(), name='deadline_details'),
    url(r'^completion/(?P<pk>[0-9]+)/$', InfoCompletionDetails.as_view(), name='completions_details'),
]
