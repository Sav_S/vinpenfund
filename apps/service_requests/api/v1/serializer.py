# -*- coding: utf-8 -*-
from rest_framework import serializers
from ...models import ServiceRequests, Deadlines, InfoCompletion


class ServiceRequestsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceRequests
        fields = ('id', 'username', 'cabinet', 'problem', 'location', 'phone', 'date_request')


class DeadlinesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Deadlines
        fields = ('id', 'request', 'responsible_employee', 'deadline')


class InfoCompletionSerializer(serializers.ModelSerializer):

    class Meta:
        model = InfoCompletion
        fields = ('id', 'request', 'date_completion', 'worklist')


class ExpandedListSerializer(serializers.ModelSerializer):
    employee = serializers.StringRelatedField(source='deadlines.responsible_employee')
    deadline = serializers.StringRelatedField(source='deadlines.deadline')
    deadline_id = serializers.StringRelatedField(source='deadlines.id')
    date_completion = serializers.StringRelatedField(source='infocompletion.date_completion')
    worklist = serializers.StringRelatedField(source='infocompletion.worklist')
    completion_id = serializers.StringRelatedField(source='infocompletion.id')
    location_value = serializers.ReadOnlyField()
    request_status = serializers.ReadOnlyField()

    # deadlines = DeadlinesSerializer('deadline')
    #request_status = serializers.SerializerMethodField('is_status')
    #def is_status(self, obj):
        #a = obj.employe + obj.deadline
        #return a

    class Meta:
        model = ServiceRequests
        fields = ('id', 'username', 'cabinet', 'problem', 'location', 'phone', 'date_request', 'employee',
                  'deadline', 'date_completion', 'worklist', 'location_value', 'request_status',
                  'deadline_id', 'completion_id')

