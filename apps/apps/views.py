from django.contrib import auth
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf


def login(request):
    c = {}
    c.update(csrf((request)))
    return render(request, 'login.html', c)


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('../')


def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect('/logged')
    else:
        return HttpResponseRedirect('/invalid')


def logged(request):
    return HttpResponseRedirect('../')


def invalid_login(request):
    return render(request, 'invalid_login.html')
