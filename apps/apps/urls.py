from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin

from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls
from wagtail.wagtailcore import urls as wagtail_urls

from .views import *

urlpatterns = [
    url(r'^django-admin/', include(admin.site.urls)),
    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^api/v1/service_requests/', include('service_requests.api.v1.urls')),
    url(r'^servicerequests/', include('service_requests.urls')),
    url(r'^search/$', 'search.views.search', name='search'),
    # auth
    url(r'^login/', login, name='login'),
    url(r'^auth/',  auth_view, name='auth'),
    url(r'^logged/', logged, name='logged'),
    url(r'^logout/', logout, name='logout'),
    url(r'^invalid/', invalid_login, name='invalid'),
    url(r'', include(wagtail_urls)),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.views.generic import TemplateView

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
